package main

import (
	"os"
	"fmt"
	"errors"
	"database/sql"
	"context"

        "github.com/aws/aws-lambda-go/lambda"
	"github.com/gomodule/redigo/redis"
	_ "github.com/lib/pq"
)

var psqlInfo string
var db *sql.DB
var conn redis.Conn
var err error

func logout(ctx context.Context, token string) (string, error) {
	if db == nil || conn == nil { 
		return "", 
		errors.New("Connection to either database failed.") 
	}
	conn.Do("DEL", "core:"+token)
	db.QueryRow("select * from logout("+token+");")
	return "OK", nil
}
func init(){
	psqlInfo = fmt.Sprintf("host=%s port=%s user=%s "+
		"password=%s dbname=%s sslmode=disable",
		os.Getenv("PGHOST"), os.Getenv("PGPORT"), 
		os.Getenv("PGUSER"), os.Getenv("PGPASSWORD"),
		os.Getenv("PGDATABASE"))

	db,err = sql.Open("postgres", psqlInfo)
	if err != nil {
		panic(err)
	}
	err = db.Ping()
	if err != nil {
		panic(err) 
	}
	conn,err = redis.Dial("tcp",
	os.Getenv("REDIS_HOST")+":"+os.Getenv("REDIS_PORT"))
	if err != nil {
		panic(err)
	}
}
func terminar(){
	if db != nil {
		db.Close();
	}
	if conn != nil {
		conn.Close()
	}
}
func main() {
        lambda.Start(logout)
	defer terminar()
}
